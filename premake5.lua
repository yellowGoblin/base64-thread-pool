-- premake5 project config --
-- changed by Mateusz Debowski

workspace "Base64_with_thread_pool_workspace"
   configurations { "Debug", "Release" }

-- APPLICATION PROJECT --
project "Base64_with_thread_pool_project"
   -- project base settings --
   kind "ConsoleApp"
   language "C++"
   
   -- target dir --
   targetdir "bin/%{cfg.buildcfg}"

   -- obj dir --
   objdir "obj/%{cfg.buildcfg}"
   
   -- include dirs --
   includedirs { "include/", "src/", "src/App/", "src/Base64/", "src/Files/", "src/Tasks/", "src/ThreadPool/", "src/Time/" }
   
   -- src files --
   files { "src/**.h", "src/**.cpp", "include/**.h" }
   
   -- lib dirs --
   libdirs { "lib/" }
   
   -- libs --
   --links { "gtestd.lib" }

   -- debug working dir --
   debugdir "bin/%{cfg.buildcfg}"
   
   filter "configurations:Debug"
      defines { "DEBUG" }
      flags { "Symbols" }

   filter "configurations:Release"
      defines { "NDEBUG" }
      flags { "Symbols" }

-- TESTS PROJECT --
project "Base64_with_thread_pool_tests"
   -- tests project settings --
   links { "Base64_with_thread_pool_project" }
   kind "ConsoleApp"
   language "C++"
   
   -- target dir --
   targetdir "tests/bin/%{cfg.buildcfg}"

   -- obj dir --
   objdir "tests/obj/%{cfg.buildcfg}"
   
   -- include dirs --
   includedirs { "include/", "src/", "tests/src/", "tests/src/Mocks/", "tests/src/Stubs/", "tests/src/Tests", "tests/include/", "src/App/", "src/Base64/", "src/Files/", "src/Tasks/", "src/ThreadPool/", "src/Time/" }
   
   -- src files --
   files { "src/**.h", "src/**.cpp", "include/**.h", "tests/src/**.h", "tests/src/**.cpp", "tests/include/**.h" }
   excludes { "src/main.cpp" }
   
   -- lib dirs --
   libdirs { "tests/lib/" }
   
   -- libs --
   links { "gtestd.lib", "gmock.lib" }
   
   -- debug working dir --
   debugdir "tests/bin/%{cfg.buildcfg}"
   
   filter "configurations:Debug"
      defines { "DEBUG" }
      flags { "Symbols" }

   filter "configurations:Release"
      defines { "NDEBUG" }
      flags { "Symbols" }
   