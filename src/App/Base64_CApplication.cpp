#include "Base64_CApplication.h"

volatile bool Base64_Application::Base64_Globals::terminationFlag = false;

BOOL WINAPI Base64_Application::Base64_Globals::signalHandler(_In_ DWORD dwCtrlType) {
	switch (dwCtrlType)
	{
	case CTRL_C_EVENT:
		std::cout << "INF: TERMINATE SIGNAL INTERCEPTED" << std::endl;
		terminationFlag = true;
		return TRUE;
	default:
		return FALSE;
	}
}
