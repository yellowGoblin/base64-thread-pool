#pragma once
#ifndef BASE_64_C_APPLICATION_H
#define BASE_64_C_APPLICATION_H

#include <Windows.h>
#include <iostream>

namespace Base64_Application
{
	namespace Base64_Globals
	{
		//! Global value which tells whether the termination signal was sent
		extern volatile bool terminationFlag;

		//! Global function which sets a global terminationFlag value to true if user pressed crtl+C.
		extern BOOL WINAPI signalHandler(_In_ DWORD dwCtrlType);
	}
}

#endif // !BASE_64_C_APPLICATION_H
