#include "Base64_CEncoderDecorer.h"
#include "Base64_CApplication.h"
#include "Base64_CFileManager.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <direct.h>

using namespace Base64_Application;
using namespace Base64_Operations;

bool CBase64EncoderDecoder::m_terminate = false;

char CBase64EncoderDecoder::decodeBase64Value(char a_inValue)
{
	static const char decoding[] = { 62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-2,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51 };
	static const char decoding_size = sizeof(decoding);
	a_inValue -= 43;
	if (a_inValue < 0 || a_inValue > decoding_size) return -1;
	return decoding[(int)a_inValue];
}

char CBase64EncoderDecoder::encodeBase64Value(char a_inValue)
{
	static const char encoding[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	if (a_inValue > 63) return '=';
	return encoding[(int)a_inValue];
}

bool CBase64EncoderDecoder::isBase64(unsigned char a_char)
{
	return (isalnum(a_char) || (a_char == '+') || (a_char == '/'));
}

bool CBase64EncoderDecoder::decodeFile(std::fstream &a_inputFile, std::fstream &a_outputFile)
{
	char buffer = NULL;     // read file's bytes to this buffer
	char rest = NULL;       // rest of the shifted fragment which is a part of the next fragment	        
	char fragment = NULL;   // fragment to decode (shifted to the left)
	char ret = NULL;        // write bytes to file from this buffer
	char currentDec = NULL; // currently decoded fragment (temp)

	enum decodingStates
	{
		STATE_A,
		STATE_B,
		STATE_C,
		STATE_D
	} state = STATE_A;

	while (a_inputFile.read(&buffer, sizeof(char)))
	{
		if (!isBase64(buffer) && buffer != '=')
		{
			std::cerr << "ERR: not valid base64 character." << std::endl;
			a_inputFile.close();
			a_outputFile.close();
			return FALSE;
		}

		if (Base64_Globals::terminationFlag)
		{
			a_inputFile.close();
			a_outputFile.close();
			return FALSE;
		}
		switch (state)
		{
		case STATE_A:
			fragment = decodeBase64Value(buffer) << 2;
			state = STATE_B;
			break;
		case STATE_B:
			currentDec = decodeBase64Value(buffer);
			if (currentDec < 0)
				break;
			fragment = fragment | (currentDec >> 4);
			rest = (currentDec & 0x0f) << 4;
			ret = fragment;
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_C;
			break;
		case STATE_C:
			currentDec = decodeBase64Value(buffer);
			if (currentDec < 0)
				break;
			fragment = (currentDec >> 2) | rest;
			rest = (currentDec & 0x03) << 6;
			ret = fragment;
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_D;
			break;
		case STATE_D:
			currentDec = decodeBase64Value(buffer);
			if (currentDec < 0)
				break;
			fragment = currentDec | rest;
			ret = fragment;
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_A;
			break;
		default:
			break;
		}
	}

	return TRUE;
}

bool CBase64EncoderDecoder::encodeFile(std::fstream &a_inputFile, std::fstream &a_outputFile)
{
	char buffer = NULL;      // read file's bytes to this buffer
	char rest = NULL;        // rest of the shifted fragment which is a part of the next fragment
	char fragment = NULL;    // fragment to encode (6 bits shifted to the right)
	char ret = NULL;         // write bytes to file from this buffer
	long counter = 0;        // counts how many bytes we already read, used for padding

	enum decodingStates
	{
		STATE_A,
		STATE_B,
		STATE_C
	} state = STATE_A;

	while (a_inputFile.read(&buffer, sizeof(char)))
	{
		if (Base64_Globals::terminationFlag)
		{
			a_inputFile.close();
			a_outputFile.close();
			return FALSE;
		}
		counter++;
		switch (state)
		{
		case STATE_A:
			fragment = (buffer & 0xFC) >> 2;
			rest = (buffer & 0x03) << 4;
			ret = encodeBase64Value(fragment);
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_B;
			break;
		case STATE_B:
			fragment = ((buffer & 0xF0) >> 4) | rest;
			rest = (buffer & 0x0F) << 2;
			ret = encodeBase64Value(fragment);
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_C;
			break;
		case STATE_C:
			fragment = ((buffer & 0xC0) >> 6) | rest;
			rest = (buffer & 0x3F);
			ret = encodeBase64Value(fragment);
			a_outputFile.write(&ret, sizeof(char));
			ret = encodeBase64Value(rest);
			a_outputFile.write(&ret, sizeof(char));
			state = STATE_A;
			break;
		default:
			break;
		}
	}	

	if (counter % 3 != 0)
	{
		char padding = NULL;
		switch (state)
		{
		case STATE_A:
			break;
		case STATE_B:
			fragment = padding | rest;
			ret = encodeBase64Value(rest);
			a_outputFile.write(&ret, sizeof(char));
			a_outputFile.write("=", sizeof(char));
			a_outputFile.write("=", sizeof(char));
			break;
		case STATE_C:
			fragment = padding | rest;
			ret = encodeBase64Value(rest);
			a_outputFile.write(&ret, sizeof(char));
			a_outputFile.write("=", sizeof(char));
			break;
		default:
			break;
		}
	}

	return TRUE;
}
