#pragma once
#ifndef BASE_64_C_ENCODER_DECODER_H
#define BASE_64_C_ENCODER_DECODER_H

#include <string>

namespace Base64_Application
{
	namespace Base64_Operations
	{
		//! This struct contains configuration which is used by Base64 task.
		struct Base64Config
		{
			//! Input file path.
			std::string filePath;

			//! Output file path.
			std::string destPath;

			//! Enum containing all base64 operation types. 
			enum OperationType
			{
				DECODE,
				ENCODE
			} //! Base64 operation type. 
			opType; 
		};

		//! Base64 encoder and decoder class.
		/*!
		This class is responsible for decoding and encoding files using base64 method.
		*/
		class CBase64EncoderDecoder
		{
		public:
			//! Base64 static decode method.
			/*!
			\param a_inputFile File stream to be decoded,
			\param a_outputFile Output file stream.
			*/
			static bool decodeFile(std::fstream &a_inputFile, std::fstream &a_outputFile);

			//! Base64 static encode method.
			/*!
			\param a_inputFile File stream to be encoded,
			\param a_outputFile Output file stream.
			*/
			static bool encodeFile(std::fstream &a_inputFile, std::fstream &a_outputFile);
		
		private:
			static bool m_terminate;

			static char encodeBase64Value(char a_inValue);
			static char decodeBase64Value(char a_inValue);
			static bool isBase64(unsigned char a_char);
		};
	}
}

#endif // !BASE_64_C_ENCODER_DECODER_H
