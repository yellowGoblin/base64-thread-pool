#pragma once
#ifndef BASE_64_C_FILE_MANAGER_H
#define BASE_64_C_FILE_MANAGER_H

#include <vector>
#include <fstream>

namespace Base64_Application
{
	namespace Base64_Files
	{
		//! This enum contains modes for file handlers opening.
		enum FileMode
		{
			//! Open file for reading.
			FILE_IN = std::ios::in,

			//! Open file for writing.
			FILE_OUT = std::ios::out,

			//! Open file for binary reading.
			FILE_IN_BIN = std::ios::in | std::ios::binary,

			//! Open file for binary writing.
			FILE_OUT_BIN = std::ios::out | std::ios::binary,

			//! Open file for reading and writing.
			FILE_IN_OUT = std::ios::in | std::ios::out,

			//! Open file for reading and writing.
			FILE_IN_OUT_BIN = std::ios::in | std::ios::out | std::ios::binary
		};

		//! File manager class.
		/*!
		This class is responsible for openning/reading/writing files.
		*/
		class CFileManager
		{
		public:
			//! A statis template method used for loading a file handler.
			/*!
			\param a_filePath A string containing file path.
			\tparam t_fileMode A mode in which to open a file handler.
			*/
			template<FileMode t_fileMode>
			static std::fstream loadFileHandler(const std::string &a_filePath)
			{
				std::fstream fileHandler;

				fileHandler.open(a_filePath, t_fileMode);
				if (!fileHandler.good())
				{
					fileHandler.close();
					std::string message = "ERR: Could not open or create a file: " + a_filePath;
					throw std::runtime_error(message);
				}

				return fileHandler;
			}
		};
	}
}

#endif // !BASE_64_C_FILE_MANAGER_H