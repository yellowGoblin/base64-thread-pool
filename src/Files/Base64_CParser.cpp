#include "Base64_CParser.h"
#include "Base64_CFileManager.h"

#include <fstream>
#include <iostream>

using namespace Base64_Application;
using namespace Base64_Files;

CParser::CParser(std::string a_filePath)
{
	readFile(a_filePath);
}

CParser::~CParser()
{

}

void CParser::readFile(std::string a_filePath)
{
	std::fstream fileHandler; 
	
	try {
		fileHandler = CFileManager::loadFileHandler<FileMode::FILE_IN>(a_filePath);
	} catch (std::exception &e) {
		throw e;
	}

	std::string oneLineData;
	while (!fileHandler.eof())
	{
		getline(fileHandler, oneLineData);
		m_paths.push_back(oneLineData);
	}	
}