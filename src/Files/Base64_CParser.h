#pragma once
#ifndef BASE_64_C_PARSER_H
#define BASE_64_C_PARSER_H

#include <string>
#include <vector>

namespace Base64_Application
{
	namespace Base64_Files
	{
		//! Text file parser class.
		/*!
		This class is responsible for reading files paths from the file specified in argument.
		*/
		class CParser
		{
		public:
			//! Text file parser class constructor.
			/*!
			\param a_filePath A path to a text file containing paths to files to be decoded/encoded.
			*/
			CParser(std::string a_filePath);

			//! Text file parser class destructor.
			~CParser();

			//! This method returns vector of read files paths.
			const std::vector<std::string> &getPaths() { return m_paths; }

		private:
			std::vector<std::string> m_paths;
			void readFile(std::string a_filePath);
		};
	}
}

#endif // !BASE_64_C_PARSER_H

