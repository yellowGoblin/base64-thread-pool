#include "Base64_CBase64Task.h"
#include "Base64_CEncoderDecorer.h"
#include "Base64_CStoper.h"
#include "Base64_CFileManager.h"
#include "Base64_CApplication.h"

#include <iostream>
#include <fstream>

using namespace Base64_Application;
using namespace Base64_Tasks;
using namespace Base64_Operations;
using namespace Base64_Time;

CBase64Task::CBase64Task(Base64Config a_config) : ITask(), m_config(a_config)
{

}

CBase64Task::~CBase64Task()
{

}

void CBase64Task::execute()
{
	std::string encodePostfix = ".b64";
	std::string taskTypeString = (m_config.opType == Base64Config::OperationType::DECODE) ? "DECODE" : "ENCODE";
	std::string outputFileName = m_config.filePath.substr(m_config.filePath.find_last_of("/\\") + 1);

	if (m_config.opType == Base64Config::OperationType::ENCODE)
	{
		outputFileName = outputFileName + encodePostfix;
	}
	if (m_config.opType == Base64Config::OperationType::DECODE)
	{
		outputFileName.erase(outputFileName.length() - encodePostfix.length());
	}

	if (Base64_Globals::terminationFlag)
	{
		return;
	}
	CStoper<std::chrono::milliseconds> threadStoper;

	std::fstream inputFile;
	std::fstream outputFile;
	try {
		inputFile = Base64_Files::CFileManager::loadFileHandler<Base64_Files::FileMode::FILE_IN_BIN>(m_config.filePath);
		outputFile = Base64_Files::CFileManager::loadFileHandler<Base64_Files::FileMode::FILE_OUT_BIN>(m_config.destPath + outputFileName);
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return;
	}

	inputFile.seekg(0, inputFile.end);
	std::streampos inputFileBytes = inputFile.tellg();
	inputFile.seekg(0, inputFile.beg);

	bool returnValue = false;

	switch (m_config.opType)
	{
	case Base64Config::OperationType::DECODE:
		returnValue = CBase64EncoderDecoder::decodeFile(inputFile, outputFile);
		break;
	case Base64Config::OperationType::ENCODE:
		returnValue = CBase64EncoderDecoder::encodeFile(inputFile, outputFile);
		break;
	default:
		std::cerr << "ERR: invalid task operation type, skipping." << std::endl;
		break;
	}

	outputFile.seekg(0, outputFile.end);
	std::streampos outputFileBytes = outputFile.tellg();
	outputFile.seekg(0, outputFile.beg);

	inputFile.close();
	outputFile.close();

	if (FALSE == returnValue)
	{
		std::remove((m_config.destPath + outputFileName).c_str());
		return;
	}

	std::cout << taskTypeString << " | IN: " << m_config.filePath << "\t| OUT: " <<
		m_config.destPath + outputFileName << "\t| SRC: " << inputFileBytes << "\t| DEST: " << outputFileBytes <<
		"\t| TIME: " << threadStoper.elapsed().count() << std::endl;
}
