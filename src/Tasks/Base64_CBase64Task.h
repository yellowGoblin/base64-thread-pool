#pragma once
#ifndef BASE_64_C_BASE_64_TASK_H
#define BASE_64_C_BASE_64_TASK_H

#include "Base64_ITask.h"
#include "Base64_CEncoderDecorer.h"

#include <string>

namespace Base64_Application
{
	namespace Base64_Tasks
	{
		//! Base64 Task class.
		/*!
		This class is used for encoding or decoding files depending on config.
		*/
		class CBase64Task : public ITask
		{
		public:
			//! Base64 task constructor.
			CBase64Task(Base64_Operations::Base64Config config);
			
			//! Base64 task destructor.
			~CBase64Task();

			//! Base64 task execute method.
			void execute();

		private:
			Base64_Operations::Base64Config m_config;
		};
	}
}

#endif // !BASE_64_C_BASE_64_TASK_H
