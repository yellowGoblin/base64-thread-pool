#include "Base64_CTaskQueue.h"
#include "Base64_CApplication.h"

using namespace Base64_Application;
using namespace Base64_Tasks;

CTaskQueue::CTaskQueue()
{
	InitializeCriticalSection(&m_critSection);
	InitializeConditionVariable(&m_condVariable);
	m_done = false;
}

CTaskQueue::~CTaskQueue()
{
	DeleteCriticalSection(&m_critSection);
}

void CTaskQueue::addTask(ITask *a_task)
{
	EnterCriticalSection(&m_critSection);
	m_tasks.push(a_task);
	WakeConditionVariable(&m_condVariable);
	LeaveCriticalSection(&m_critSection);
}

ITask *CTaskQueue::getTask()
{
	ITask *task = NULL;

	EnterCriticalSection(&m_critSection);

	if(!m_done && m_tasks.size() == 0)
	{
		SleepConditionVariableCS(&m_condVariable, &m_critSection, INFINITE);
	}

	if (m_done && m_tasks.size() == 0)
	{
		task = NULL;
	}
	else
	{
		if (!m_tasks.empty())
		{
			task = m_tasks.front();
			m_tasks.pop();
		}
		else
		{
			task = NULL;
		}
	}
		
	LeaveCriticalSection(&m_critSection);

	return task;
}

void CTaskQueue::setDone()
{
	EnterCriticalSection(&m_critSection);
	m_done = true;	
	WakeAllConditionVariable(&m_condVariable);
	LeaveCriticalSection(&m_critSection);
}