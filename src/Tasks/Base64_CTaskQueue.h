#pragma once
#ifndef BASE_64_C_TASK_QUEUE_H
#define BASE_64_C_TASK_QUEUE_H

#include "Base64_ITask.h"

#include <queue>
#include <Windows.h>
#include <iostream>

namespace Base64_Application 
{
	namespace Base64_Tasks
	{
		//! Task queue class.
		/*!
		std::queue wrapper for tasks with critical section for multithreading.
		*/
		class CTaskQueue
		{
		public:
			//! A constructor used for task queue initialization.
			/*!
			Initializes critical section;
			*/
			CTaskQueue();

			//! A destructor responsible for releasing resources such as pointers.
			/*!
			Deletes critical section.
			*/
			~CTaskQueue();

			//! Method used for adding new task to the queue.
			/*!
			\param a_Task A pointer to new task.
			*/
			void addTask(ITask *a_Task);

			//! Method used for retrieving next task from the queue
			ITask *getTask();

			//! Method which marks queue as done.
			void setDone();

		private:
			std::queue<ITask*> m_tasks;
			CRITICAL_SECTION m_critSection;
			CONDITION_VARIABLE m_condVariable;
			volatile bool m_done;
		};
	}
}

#endif // !BASE_64_C_TASK_QUEUE_H
