#pragma once
#ifndef BASE_64_I_TASK_H
#define BASE_64_I_TASK_H

namespace Base64_Application
{
	namespace Base64_Tasks
	{
		//! Task interface class.
		/*!
			Every user defined task should inherits from this class and overload it's pure virtual methods.
		*/
		class ITask
		{
		public:
			//! Task interface class constructor.
			ITask() {}

			//! Task interface class virtual destructor.
			virtual ~ITask() {}

			//! Execute method for each individual thread to call. Every user defined task should implement this method.
			virtual void execute() = 0;
		};
	}
}

#endif // !BASE_64_I_TASK_H

