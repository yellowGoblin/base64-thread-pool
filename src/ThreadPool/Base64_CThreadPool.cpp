#include "Base64_CThreadPool.h"
#include "Base64_ITask.h"
#include "Base64_CTaskQueue.h"
#include "Base64_CApplication.h"

#include <process.h>
#include <stdexcept>
#include <iostream>
#include <atomic>

using namespace Base64_Application;
using namespace Base64_Thread_Pool;
using namespace Base64_Tasks;

DWORD WINAPI workingThreadBase64Function(LPVOID a_taskQueue)
{
	ITask *task = NULL;
	CTaskQueue *queue = NULL;
	queue = (CTaskQueue*)a_taskQueue;
	if (!queue) {
		std::cerr << "ERR: task queue argument error, skipping task." << std::endl;
		return 0;
	}

	while (task = queue->getTask())
	{
		task->execute();
		delete task;
		task = NULL;
	}

	return 0;
}

CThreadPool::CThreadPool(int a_nThreads) : m_threads(NULL)
{
	try {
		initThreads(a_nThreads);
	}
	catch (std::exception &e) {
		throw e;
	}
}

CThreadPool::~CThreadPool()
{

}

void CThreadPool::addTask(ITask *a_task)
{
	m_taskQueue.addTask(a_task);
}

void CThreadPool::initThreads(int a_newAmount)
{
	if (a_newAmount <= 0)
	{
		m_nThreads = MIN_THREADS;
		std::cout << "INF: To small number of threads, setting to min value (1)." << std::endl;
	}
	else if (a_newAmount > MAX_THREADS)
	{
		m_nThreads = MAX_THREADS;
		std::cout << "INF: To large number of threads, setting to max value (60)." << std::endl;
	}
	else
	{
		m_nThreads = a_newAmount;
	}

	try {
		m_threads = new HANDLE[m_nThreads];
	}
	catch (std::exception &e) {
		throw e;
	}

	for (int i = 0; i < m_nThreads; i++)
	{
		m_threads[i] = CreateThread(NULL, 0, workingThreadBase64Function, (void*)&m_taskQueue, 0, NULL);
		if (NULL == m_threads[i])
		{
			delete[] m_threads;
			throw std::runtime_error("ERR: threads creating error.");
		}
	}

	std::cout << "INF: Working threads: " << m_nThreads << std::endl;
}

void CThreadPool::closeHandlers()
{
	DWORD waitForObjectsReturnValue = WaitForMultipleObjects(m_nThreads, m_threads, TRUE, INFINITE);
	if (WAIT_FAILED == waitForObjectsReturnValue)
	{
		std::cerr << "ERR: Wait function failed." << std::endl;
	}

	for (int i = 0; i < m_nThreads; ++i)
	{
		CloseHandle(m_threads[i]);
		m_threads[i] = NULL;
	}

	delete[] m_threads;
	m_threads = NULL;

	std::cout << "INF: Threads closed." << std::endl;
}
