#pragma once
#ifndef BASE_64_C_Base64_Thread_Pool_H
#define BASE_64_C_Base64_Thread_Pool_H

#include "Base64_CTaskQueue.h"
#include "Base64_ITask.h"

#include <Windows.h>
#include <vector>

namespace Base64_Application
{
	namespace Base64_Thread_Pool
	{
		//! Thread pool class.
		/*! 
		This class is responsible for thread pool operations such as assigning new tasks to free working threads.
		*/
		class CThreadPool
		{
		public:
			//! Class constructor used for thread pool initialization.
			/*!
			\param a_nThreads Amount of threads to initialize (default = 1).
			*/
			CThreadPool(int a_nThreads = 1);

			//! Class destructor.
			~CThreadPool();

			//! A method used for adding new task to task queue.
			/*!
			\param a_task A pointer to new task.
			*/
			void addTask(Base64_Tasks::ITask *a_task);

			//! Method which marks this threadpool's queue as done.
			inline void setQueueDone() { m_taskQueue.setDone(); }

			//! Method used for setting thread pool's thread amount.
			/*!
			\param a_newAmount New amont of threads.
			*/
			void initThreads(int a_newAmount);

			//! Method used for stopping and clearing threads and resources.
			void closeHandlers();

		protected:
			//!Task queue member
			Base64_Tasks::CTaskQueue m_taskQueue;

			//! Threads handlers
			HANDLE *m_threads;

			//! Number of threads
			int m_nThreads;

			//! Max threads number
			static const int MIN_THREADS = 1;

			//! Min threads number
			static const int MAX_THREADS = 60;
		};
	}
}

#endif // !BASE_64_C_Base64_Thread_Pool_H
