#pragma once
#ifndef BASE_64_C_STOPER_H
#define BASE_64_C_STOPER_H

#include <type_traits>
#include <chrono>

namespace Base64_Application
{
	namespace Base64_Time
	{
		//! This class is responsible for time measurement.
		/*!
		\tparam t_resolution Time resolution template parameter.
		*/
		template <typename t_resolution>
		class CStoper
		{
		public:
			//! A constructor used for stoper initialization.
			CStoper()
			{
				reset();
			}

			//! A method used for stoper reseting.
			void reset()
			{
				m_resetTime = m_clock.now();
			}

			//! A method which returns time elapsed.
			t_resolution elapsed()
			{
				return std::chrono::duration_cast<t_resolution>(m_clock.now() - m_resetTime);
			}

		private:
			std::chrono::high_resolution_clock m_clock;
			std::chrono::high_resolution_clock::time_point m_resetTime;
		};
	}
}

#endif // !BASE_64_C_STOPER_H
