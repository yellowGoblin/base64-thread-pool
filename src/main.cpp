﻿#include "Base64_CThreadPool.h"
#include "Base64_CStoper.h"
#include "Base64_CParser.h"
#include "Base64_CApplication.h"
#include "Base64_CEncoderDecorer.h"
#include "Base64_CBase64Task.h"

#include "optionParser\optionparser.h"

#include <Windows.h>
#include <process.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <atlstr.h>

using namespace Base64_Application;
using namespace Base64_Thread_Pool;
using namespace Base64_Tasks;
using namespace Base64_Time;
using namespace Base64_Files;

enum  optionIndex { UNKNOWN, HELP, THREADS, OUTDIR, DEC, ENC };
const option::Descriptor usage[] =
{
	{ UNKNOWN, 0,"" , ""    ,option::Arg::None,         "--| USAGE |--\n\nOptions:" },
	{ HELP,    0, "h" , "help", option::Arg::None,      "  --help, -h  \t - Display help and exit program." },
	{ THREADS, 0, "t",  "thds",  option::Arg::Optional, "  --thrd, -t\"nThreads\" \t - Change threads amount." },
	{ OUTDIR,  0, "o",  "outd",  option::Arg::Optional, "  --outd, -o\"out/dir/\" \t - Change output directory." },
	{ DEC,     0, "d",  "dec",   option::Arg::Optional, "  --dec,  -d\"fileList\" \t - Decode files stored in 'fileList'." },
	{ ENC,     0, "e",  "enc",   option::Arg::Optional, "  --enc,  -e\"fileList\" \t - Encode files stored in 'fileList'." },
	{ UNKNOWN, 0, "" ,  ""   ,  option::Arg::None, ""},
	{ 0,0,0,0,0,0 }
};

int nThreads = 4;
std::string stringValue;
std::stringstream streamValue;
std::string fileListPath;
std::string outDirPath;
Base64_Operations::Base64Config::OperationType opType;

int parseArguments(int argc, char **argv)
{
	argc -= (argc>0); argv += (argc>0);

	option::Option *options = NULL;
	option::Option *buffer = NULL; 

	option::Stats stats(usage, argc, argv);
	try {
		options = new option::Option[stats.options_max];
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return -1;
	}

	try {
		buffer = new option::Option[stats.buffer_max];
	}
	catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		delete[] options;
		return -1;
	}
	
	option::Parser parse(usage, argc, argv, options, buffer);

	if (argc == 0 || (!options[DEC].arg && !options[ENC].arg) || !options[OUTDIR].arg)
	{
		option::printUsage(std::cout, usage);
		delete[] options;
		delete[] buffer;
		return -1;
	}
	if (parse.error())
	{
		delete[] options;
		delete[] buffer;
		return -1;
	}

	if (options[HELP])
	{
		option::printUsage(std::cout, usage);
		delete[] options;
		delete[] buffer;
		return -1;
	}

	for (int i = 0; i < parse.optionsCount(); ++i)
	{
		option::Option& opt = buffer[i];
		switch (opt.index())
		{
		case HELP:
			break;
		case OUTDIR:
			if (!opt.arg)
			{
				option::printUsage(std::cout, usage);
				delete[] options;
				delete[] buffer;
				return -1;
			}
			outDirPath = opt.arg;
			break;
		case ENC:
			if (!opt.arg)
			{
				option::printUsage(std::cout, usage);
				delete[] options;
				delete[] buffer;
				return -1;
			}
			fileListPath = opt.arg;
			opType = Base64_Operations::Base64Config::OperationType::ENCODE;
			break;
		case DEC:
			if (!opt.arg)
			{
				option::printUsage(std::cout, usage);
				delete[] options;
				delete[] buffer;
				return -1;
			}
			fileListPath = opt.arg;
			opType = Base64_Operations::Base64Config::OperationType::DECODE;
			break;
		case THREADS:
			if (!opt.arg)
			{
				option::printUsage(std::cout, usage);
				delete[] options;
				delete[] buffer;
				return -1;
			}
			stringValue = opt.arg;
			streamValue << stringValue;
			streamValue >> nThreads;
			break;
		case UNKNOWN:
			break;

		default:
			break;
		}
	}

	delete[] options;
	delete[] buffer;

	return 0;
}

int main(int argc, char **argv)
{
	if (parseArguments(argc, argv) < 0)
		return -1;

	SetConsoleCtrlHandler(Base64_Globals::signalHandler, TRUE);

	CThreadPool *threadPool = NULL;
	CParser *parser = NULL;

	CStoper<std::chrono::milliseconds> stoper;

	try {
		parser = new CParser(fileListPath);
	} catch (std::exception &e) {
		std::cerr << e.what() << std::endl;
		return -1;
	}

	try {
		threadPool = new CThreadPool(nThreads);
	} catch (std::exception &e) {
		delete parser;
		std::cerr << e.what() << std::endl;
		return -1;
	}

	CString cStringOutputPath(outDirPath.c_str());
	DWORD creatingDirectoryReturn = CreateDirectory(cStringOutputPath, NULL);
	if (NULL == creatingDirectoryReturn)
	{
		if (!PathFileExists(cStringOutputPath))
		{
			outDirPath = ".";
			std::cerr << "ERR: Directory creation failed, saving in current." << std::endl;
		}
	}

	for (size_t i = 0; i < parser->getPaths().size(); ++i)
	{
		threadPool->addTask(new CBase64Task({ parser->getPaths()[i], outDirPath, opType }));
	}

	threadPool->setQueueDone();
	threadPool->closeHandlers();

	std::cout << "Elapsed time: " << stoper.elapsed().count() << std::endl;

	delete threadPool;
	delete parser;

	return 0;
}
