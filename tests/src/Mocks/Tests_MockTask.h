#pragma once
#ifndef TESTS_MOCK_TEST_H
#define TESTS_MOCK_TEST_H

#include "gmock\gmock.h"

#include "Base64_ITask.h"

using namespace Base64_Application;
using namespace Base64_Tasks;

namespace Tests
{
	class MockTask : public ITask
	{
	public:
		MOCK_METHOD0(execute, void());
	};
}

#endif // !TESTS_MOCK_TEST_H