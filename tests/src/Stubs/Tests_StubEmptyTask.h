#pragma once
#ifndef TESTS_STUB_EMPTY_TASK
#define TESTS_STUB_EMPTY_TASK

#include "Base64_ITask.h"
#include "Base64_CApplication.h"

using namespace Base64_Application;
using namespace Base64_Tasks;
using namespace Base64_Globals;

namespace Tests
{
	class EmptyTask : public ITask
	{
	public:
		void execute()
		{
			for (int i = 0; i < 100000; ++i)
			{
				Sleep(100);
				if (terminationFlag)
				{	
					std::cout << "empty thread is returning..." << std::endl;
					return;
				}
			}
		}
	};
}

#endif // !TESTS_STUB_EMPTY_TASK
