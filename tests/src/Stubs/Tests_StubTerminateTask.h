#pragma once
#ifndef TESTS_STUB_TERMINATE_TASK
#define TESTS_STUB_TERMINATE_TASK

#include "Base64_ITask.h"
#include "Base64_CApplication.h"

using namespace Base64_Application;
using namespace Base64_Tasks;
using namespace Base64_Globals;

namespace Tests
{
	class TerminateTask : public ITask
	{
	public:
		void execute()
		{
			for (int i = 0; i < 100000; ++i)
			{
				if (terminationFlag)
				{
					std::cout << "terminate thread is returning..." << std::endl;
					return;
				}
				if (500 == i)
				{
					terminationFlag = true;
				}
			}
		}
	};
}

#endif // !TESTS_STUB_TERMINATE_TASK
