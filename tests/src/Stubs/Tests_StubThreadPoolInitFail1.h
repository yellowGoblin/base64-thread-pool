#pragma once
#ifndef TESTS_STUB_THREAD_POOL_INIT_FAIL_1_H
#define TESTS_STUB_THREAD_POOL_INIT_FAIL_1_H

#include "Base64_CThreadPool.h"

using namespace Base64_Application;
using namespace Base64_Thread_Pool;

namespace Tests
{
	class StubThreadPoolInitFail1 : public CThreadPool
	{
	public:
		void initThreads(int a_newAmount) { throw std::runtime_error("MEMORY ALOCATION FAILED THROWN BY INIT THREADS METHOD - OK"); }
	};
}

#endif // !TESTS_STUB_THREAD_POOL_INIT_FAIL_1_H
