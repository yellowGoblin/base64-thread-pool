#pragma once
#ifndef TESTS_STUB_THREAD_POOL_INIT_FAIL_2_H
#define TESTS_STUB_THREAD_POOL_INIT_FAIL_2_H

#include "Base64_CThreadPool.h"

using namespace Base64_Application;
using namespace Base64_Thread_Pool;

namespace Tests
{
	class StubThreadPoolInitFail2 : public CThreadPool
	{
	public:
		void initThreads(int a_newAmount)
		{
			if (a_newAmount <= 0)
			{
				a_newAmount = MIN_THREADS;
				std::cout << "INF: To small number of threads, setting to min value (1)." << std::endl;
			}
			else if (a_newAmount > MAX_THREADS)
			{
				a_newAmount = MAX_THREADS;
				std::cout << "INF: To large number of threads, setting to max value (60)." << std::endl;
			}

			m_nThreads = a_newAmount;
			try {
				m_threads = new HANDLE[m_nThreads];
			}
			catch (std::exception &e) {
				throw e;
			}

			for (int i = 0; i < m_nThreads; i++)
			{
				m_threads[i] = NULL;
				if (m_threads[i] == NULL)
				{
					delete[] m_threads;
					throw std::runtime_error("ERR: threads creating error.");
				}
			}
		}
	};
}
#endif // !TESTS_STUB_THREAD_POOL_INIT_FAIL_2_H
