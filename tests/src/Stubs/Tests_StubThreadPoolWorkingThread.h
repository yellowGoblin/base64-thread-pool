#pragma once
#ifndef TESTS_STUB_THREAD_POOL_WORKING_THREAD_H
#define TESTS_STUB_THREAD_POOL_WORKING_THREAD_H

#include "Base64_CThreadPool.h"

using namespace Base64_Application;
using namespace Base64_Thread_Pool;

namespace Tests
{
	CRITICAL_SECTION g_consoleCritSection;

	DWORD WINAPI stubWorkingThreadBase64Function(LPVOID param)
	{
		EnterCriticalSection(&g_consoleCritSection);
		std::cout << "Thread: " << GetCurrentThreadId() << " is sleeping..." << std::endl;
		Sleep(10);
		LeaveCriticalSection(&g_consoleCritSection);
		return 0;
	}

	class StubThreadPool : public CThreadPool
	{
	public:
		void initThreads(int a_newAmount)
		{
			InitializeCriticalSection(&g_consoleCritSection);
			if (a_newAmount <= 0)
			{
				a_newAmount = MIN_THREADS;
				std::cout << "INF: To small number of threads, setting to min value (1)." << std::endl;
			}
			else if (a_newAmount > MAX_THREADS)
			{
				a_newAmount = MAX_THREADS;
				std::cout << "INF: To large number of threads, setting to max value (60)." << std::endl;
			}

			m_nThreads = a_newAmount;
			try {
				m_threads = new HANDLE[m_nThreads];
			}
			catch (std::exception &e) {
				throw e;
			}

			for (int i = 0; i < m_nThreads; i++)
			{
				m_threads[i] = CreateThread(NULL, 0, stubWorkingThreadBase64Function, NULL, 0, NULL);
				if (NULL == m_threads[i])
				{
					delete[] m_threads;
					throw std::runtime_error("ERR: threads creating error.");
				}
			}

			std::cout << "INF: Working threads: " << m_nThreads << std::endl;
		}
	};
}

#endif // !TESTS_STUB_THREAD_POOL_WORKING_THREAD_H
