﻿//#pragma once
//#pragma once
//#ifndef TEST_GLOBALS_H
//#define TEST_GLOBALS_H

#include "gtest\gtest.h"

#include "Base64_CFileManager.h"
#include "Base64_CEncoderDecorer.h"

using namespace Base64_Application;
using namespace Base64_Files;
using namespace Base64_Operations;

const std::string encodeThis = "dG  Fla CAoZU1ldGU2TiBJem9uIGZvNCBsZGRzIHJlcmljLno2c2VydHJZZyBhaWJtIG9kb2Nyd2kwYW51YWEgZTZyZ2VzciBvTXQgYWlzTm4gIHlsMHQgQk5vZGVhbnU2IGVvYW9hdGRyNGkgcnRuZXRyMU4gbnNkIHQgbnNkaS5wTmUgdSBpbmlvaSBmaTR0c2F5cmVpc2NlZiBjaWNzdHNhRWVhb0J5cyByb2VuZGRucmlzciBUcm9sZXNuQXN0dGNlZSB uYWVvdHBjcm1uaXAgdG4gaGVYZSAuMnV0ZWVuIHJRaSBkNE1yIGQgc25yWG15ZnRkc2JpRWRpbmFzZGQgZSBhdGVlamV5Z2hkIGhzIGdhZXQgblFzbmdpbG4gZSBlbm9ndGVpeSBlaXRhdXNhbWNlaWRJb2VuZ25laTJ1bmdZaWMgaWl0dGggdXJuYU5NZmVhZGQgZSAoYmEgYWVncHJ0Z3N2ZCBuNjJzcmogeCByZiAgb3RuZG9NbiB5IGEgblRtYWVuICBkZHNkeGFyZy50ZWRjZU0gaSBvaHIgemVlaWFuYWVUZCAgZWNhdXlNdCBpaWhwZWFUdCBhIG4gZSBOIGNpNGVyYUQgYW9heWkgZURlb2NhZHRlaW9UIGFjRCBoVC50ZXNzYXNjVG9nbWFub3N0Lmh0YWl0bmxleHNkdG5ybWxnbCAgIGUgYXJZbmFlICAgaW5haGlnZXRpYmNsTm5lICA2dWNtbm9lZW5tc2hvSXRkYXQgZCBhdHRuciBzZ21zaGljdXNVb2FhaWRlVGNjbnRJZWZoYWFlIER0bmlCZWYgZWEgaXlhc2FCY2Vlck1odXNvb2F0IGVyaHIgZGQ0aWhhaW5kIFRlbnNhcnRhICByaWVwaXNhIG5yZHNlZHJZamFndHNobiAgRW5uIHRzb2huc25uIFEgY29ueGVubmdhYWUgeWkgICA2bCBlZWMgYW50byBnTiBvIHhkaG5zcmN0Nm9lb2RhNnN0Y2F0YXRUIHRhIGVzc3lBWWVvd0Qgbm5UYW9vZSByIGloc2xyZXJuIHJndVFsb2NvciAgYWRidERvbWludDFCaXNhNmlvb2VpIC5FcnBkaGluIDRhaG5ubzFpYi5kIGVlbWF0ZGFhaGF0d24gdHMgRGU9ZmUgY2plIHRucHMgZTQwbyAgTW1hdG50TSBlIGRmIE1udG90c2FtYXJpcmU9YyBlaHBlZnRvZyBhIGVpYWVyQXB0ZSB0aXRkZSB0bSggcnNudCAsIHVkZ2dlZWRnZWdlIFRzZHMgc29UZWhlZCBsbiAgY2hpICAgIG4gc2V0dHRubg==";
const std::string decodeThis = "ZEcgIEZsYSBDQW9aVTFsZEdVMlRpQkplbTl1SUdadk5DQnNaR1J6SUhKbGNtbGpMbm8yYzJWeWRISlpaeUJoYVdKdElHOWtiMk55ZDJrd1lXNTFZV0VnWlRaeVoyVnpjaUJ2VFhRZ1lXbHpUbTRnSUhsc01IUWdRazV2WkdWaGJuVTJJR1Z2WVc5aGRHUnlOR2tnY25SdVpYUnlNVTRnYm5Oa0lIUWdibk5rYVM1d1RtVWdkU0JwYm1sdmFTQm1hVFIwYzJGNWNtVnBjMk5sWmlCamFXTnpkSE5oUldWaGIwSjVjeUJ5YjJWdVpHUnVjbWx6Y2lCVWNtOXNaWE51UVhOMGRHTmxaU0IgdVlXVnZkSEJqY20xdWFYQWdkRzRnYUdWWVpTQXVNblYwWldWdUlISlJhU0JrTkUxeUlHUWdjMjV5V0cxNVpuUmtjMkpwUldScGJtRnpaR1FnWlNCaGRHVmxhbVY1WjJoa0lHaHpJR2RoWlhRZ2JsRnpibWRwYkc0Z1pTQmxibTluZEdWcGVTQmxhWFJoZFhOaGJXTmxhV1JKYjJWdVoyNWxhVEoxYm1kWmFXTWdhV2wwZEdnZ2RYSnVZVTVOWm1WaFpHUWdaU0FvWW1FZ1lXVm5jSEowWjNOMlpDQnVOakp6Y21vZ2VDQnlaaUFnYjNSdVpHOU5iaUI1SUdFZ2JsUnRZV1Z1SUNCa1pITmtlR0Z5Wnk1MFpXUmpaVTBnYVNCdmFISWdlbVZsYVdGdVlXVlVaQ0FnWldOaGRYbE5kQ0JwYVdod1pXRlVkQ0JoSUc0Z1pTQk9JR05wTkdWeVlVUWdZVzloZVdrZ1pVUmxiMk5oWkhSbGFXOVVJR0ZqUkNCb1ZDNTBaWE56WVhOalZHOW5iV0Z1YjNOMExtaDBZV2wwYm14bGVITmtkRzV5Yld4bmJDQWdJR1VnWVhKWmJtRmxJQ0FnYVc1aGFHbG5aWFJwWW1Oc1RtNWxJQ0EyZFdOdGJtOWxaVzV0YzJodlNYUmtZWFFnWkNCaGRIUnVjaUJ6WjIxemFHbGpkWE5WYjJGaGFXUmxWR05qYm5SSlpXWm9ZV0ZsSUVSMGJtbENaV1lnWldFZ2FYbGhjMkZDWTJWbGNrMW9kWE52YjJGMElHVnlhSElnWkdRMGFXaGhhVzVrSUZSbGJuTmhjblJoSUNCeWFXVndhWE5oSUc1eVpITmxaSEpaYW1GbmRITm9iaUFnUlc1dUlIUnpiMmh1YzI1dUlGRWdZMjl1ZUdWdWJtZGhZV1VnZVdrZ0lDQTJiQ0JsWldNZ1lXNTBieUJuVGlCdklIaGthRzV6Y21OME5tOWxiMlJoTm5OMFkyRjBZWFJVSUhSaElHVnpjM2xCV1dWdmQwUWdibTVVWVc5dlpTQnlJR2xvYzJ4eVpYSnVJSEpuZFZGc2IyTnZjaUFnWVdSaWRFUnZiV2x1ZERGQ2FYTmhObWx2YjJWcElDNUZjbkJrYUdsdUlEUmhhRzV1YnpGcFlpNWtJR1ZsYldGMFpHRmhhR0YwZDI0Z2RITWdSR1U5Wm1VZ1kycGxJSFJ1Y0hNZ1pUUXdieUFnVFcxaGRHNTBUU0JsSUdSbUlFMXVkRzkwYzJGdFlYSnBjbVU5WXlCbGFIQmxablJ2WnlCaElHVnBZV1Z5UVhCMFpTQjBhWFJrWlNCMGJTZ2djbk51ZENBc0lIVmtaMmRsWldSblpXZGxJRlJ6WkhNZ2MyOVVaV2hsWkNCc2JpQWdZMmhwSUNBZ0lHNGdjMlYwZEhSdWJnPT0=";
const std::string decodeThisBad = "ZEcgIEZsYSBDQW9aVTFsZEdVMlRpQęękplbTl1SUdadą^^ćk5DQnNaR1J6SUhKbGNtbGpMbm8yYzJWeWRISlpaeUJoYVdKdElHOWtiMk55ZDJrd1lXNTFZV0VnWlRaeVoyVnpjaUJ2VFhRZ1lXbHpUbTRnSUhsc01IUWdRazV2WkdWaGJuVTJJR1Z2WVc5aGRHUnlOR2tnY25SdVpYUnlNVTRnYm5Oa0lIUWdibk5rYVM1d1RtVWdkU0JwYm1sdmFTQm1hVFIwYzJGNWNtVnBjMk5sWmlCamFXTnpkSE5oUldWaGIwSjVjeUJ5YjJWdVpHUnVjbWx6Y2lCVWNtOXNaWE51UVhOMGRHTmxaU0IgdVlXVnZkSEJqY20xdWFYQWdkRzRnYUdWWVpTQXVNblYwWldWdUlISlJhU0JrTkUxeUlHUWdjMjV5V0cxNVpuUmtjMkpwUldScGJtRnpaR1FnWlNCaGRHVmxhbVY1WjJoa0lHaHpJR2RoWlhRZ2JsRnpibWRwYkc0Z1pTQmxibTluZEdWcGVTQmxhWFJoZFhOaGJXTmxhV1JKYjJWdVoyNWxhVEoxYm1kWmFXTWdhV2wwZEdnZ2RYSnVZVTVOWm1WaFpHUWdaU0FvWW1FZ1lXVm5jSEowWjNOMlpDQnVOakp6Y21vZ2VDQnlaaUFnYjNSdVpHOU5iaUI1SUdFZ2JsUnRZV1Z1SUNCa1pITmtlR0Z5Wnk1MFpXUmpaVTBnYVNCdmFISWdlbVZsYVdGdVlXVlVaQ0FnWldOaGRYbE5kQ0JwYVdod1pXRlVkQ0JoSUc0Z1pTQk9JR05wTkdWeVlVUWdZVzloZVdrZ1pVUmxiMk5oWkhSbGFXOVVJR0ZqUkNCb1ZDNTBaWE56WVhOalZHOW5iV0Z1YjNOMExtaDBZV2wwYm14bGVITmtkRzV5Yld4bmJDQWdJR1VnWVhKWmJtRmxJQ0FnYVc1aGFHbG5aWFJwWW1Oc1RtNWxJQ0EyZFdOdGJtOWxaVzV0YzJodlNYUmtZWFFnWkNCaGRIUnVjaUJ6WjIxemFHbGpkWE5WYjJGaGFXUmxWR05qYm5SSlpXWm9ZV0ZsSUVSMGJtbENaV1lnWldFZ2FYbGhjMkZDWTJWbGNrMW9kWE52YjJGMElHVnlhSElnWkdRMGFXaGhhVzVrSUZSbGJuTmhjblJoSUNCeWFXVndhWE5oSUc1eVpITmxaSEpaYW1GbmRITm9iaUFnUlc1dUlIUnpiMmh1YzI1dUlGRWdZMjl1ZUdWdWJtZGhZV1VnZVdrZ0lDQTJiQ0JsWldNZ1lXNTBieUJuVGlCdklIaGthRzV6Y21OME5tOWxiMlJoTm5OMFkyRjBZWFJVSUhSaElHVnpjM2xCV1dWdmQwUWdibTVVWVc5dlpTQnlJR2xvYzJ4eVpYSnVJSEpuZFZGc2IyTnZjaUFnWVdSaWRFUnZiV2x1ZERGQ2FYTmhObWx2YjJWcElDNUZjbkJrYUdsdUlEUmhhRzV1YnpGcFlpNWtJR1ZsYldGMFpHRmhhR0YwZDI0Z2RITWdSR1U5Wm1VZ1kycGxJSFJ1Y0hNZ1pUUXdieUFnVFcxaGRHNTBUU0JsSUdSbUlFMXVkRzkwYzJGdFlYSnBjbVU5WXlCbGFIQmxablJ2WnlCaElHVnBZV1Z5UVhCMFpTQjBhWFJrWlNCMGJTZ2djbk51ZENBc0lIVmtaMmRsWldSblpXZGxJRlJ6WkhNZ2MyOVVaV2hsWkNCc2JpQWdZMmhwSUNBZ0lHNGdjMlYwZEhSdWJnPT0=";

TEST(Base64, encodeFile)
{	
	// PRECONDITIONS
	// data to encode, streams exist
	std::ofstream inputStream("b64_in"); // new input file
	inputStream << encodeThis; // pass string to encode to the file
	inputStream.close(); // clean up the file
	std::fstream inputFile("b64_in"); // open file to read
	std::fstream outputFile("b64_out", FileMode::FILE_OUT_BIN); // create new output file to store output data
	std::string outputString; // prepare output string

	// TC SCENARIO
	// properly encode data from input file and save it to new output file
	bool ret = CBase64EncoderDecoder::encodeFile(inputFile, outputFile); // encode input file and save result in output file

	inputFile.close(); // close input file
	outputFile.close(); // close output file
	std::ifstream outputStream("b64_out"); // open output file to write output data
	std::getline(outputStream, outputString); // get encoded data from the file
	outputStream.close(); // clean up
	std::remove("b64_in"); // clean up
	std::remove("b64_out"); // clean up

	EXPECT_EQ(outputString, decodeThis); // expect valid result
	EXPECT_TRUE(ret); // encodeFile should return true

	// POSTCONDITIONS
	// input stream is encoded and saved in output stream, function returns true
}

TEST(Base64, goodDecodeFile)
{
	// PRECONDITIONS
	// valid data to decode, streams exist
	std::ofstream inputStream("b64_in"); // new input file
	inputStream << decodeThis; // pass string to decode to the file
	inputStream.close(); // clean up
	std::fstream inputFile("b64_in"); // open file to read
	std::fstream outputFile("b64_out", FileMode::FILE_OUT_BIN); // create new output file to store decoded data
	std::string outputString; // prepare output string

	// TC SCENARIO
	// properly decode data from good input file and save it to new output file
	bool ret = CBase64EncoderDecoder::decodeFile(inputFile, outputFile); // decode input file and save result in output file

	inputFile.close(); // clean up
	outputFile.close(); // clean up
	std::ifstream outputStream("b64_out"); // open output file to write output data
	std::getline(outputStream, outputString); // get decoded data from the file
	outputStream.close(); // clean up
	std::remove("b64_in"); // clean up
	std::remove("b64_out"); // clean up

	EXPECT_EQ(outputString, encodeThis); // expect valid result
	EXPECT_TRUE(ret); // decodeFile should return true

	// POSTCONDITIONS
	// input stream is decoded and saven in output stream, function returns true
}

TEST(Base64, badDecodeFile)
{
	// PRECONDITIONS
	// file to decode is not valid, streams exist
	std::ofstream inputStream("b64_in"); // create new input file
	inputStream << decodeThisBad; // pass string to encode to the file
	inputStream.close(); // clean up
	std::fstream inputFile("b64_in"); // open file to read
	std::fstream outputFile("b64_out", FileMode::FILE_OUT_BIN); // create new output file
	std::string outputString; // prepare output string

	// TC SCENARIO
	// notify user that file that is currently being decoded is not valid base64
	bool ret = CBase64EncoderDecoder::decodeFile(inputFile, outputFile); // decode input file and save result in output file

	inputFile.close(); // clean up
	outputFile.close(); // clean up
	std::ifstream outputStream("b64_out"); // open output file to write data
	std::getline(outputStream, outputString); // get decoded data from the file
	outputStream.close(); // clean up
	std::remove("b64_in"); // clean up
	std::remove("b64_out"); // clean up

	EXPECT_FALSE(ret); // we now it should return false

	// POSTCONDITIONS
	// user is notified that the input stream file is not valid base64, function returns false
}
