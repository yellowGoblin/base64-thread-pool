#include "gtest\gtest.h"

#include "Base64_CFileManager.h"

using namespace Base64_Application;
using namespace Base64_Files;

TEST(FileManager, openExistingFile)
{
	// PRECONDITIONS
	// file name is delivered
	const std::string fileName = "testFile.txt"; // set file name

	// TC SCENARIO
	// deliever valid file handler when file path is correct
	std::fstream fileHandler = CFileManager::loadFileHandler<FileMode::FILE_IN>(fileName); // load existing file

	// POSTCONDITIONS
	// valid file handler is returned from the function
}

TEST(FileManager, doNotOpenNonExistingFile)
{
	// PRECONDITIONS
	// file name is delivered
	const std::string fileName = ""; // set file name to empty
	std::fstream fileHandler; // declare empty file handler

	// TC SCENARIO
	// do not diliever invalid file handler when file path or file itself is invalid
	try {
		fileHandler = CFileManager::loadFileHandler<FileMode::FILE_IN>(fileName); // try to load unexisting file
		FAIL(); // fail the test when file handler is given anyway
	}
	catch (std::exception) {}

	// POSTCONDITIONS
	// exception is thrown, file handler is not returned, user is notified
}
