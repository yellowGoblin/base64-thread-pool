#include "gtest\gtest.h"

#include "Base64_CParser.h"

using namespace Base64_Application;
using namespace Base64_Files;

TEST(Parser, openAndParseExistingFile)
{
	// PRECONDITIONS
	// parser data is delivered, file exist, there are paths stored in the file
	const std::string parserFileName = "testFile.txt"; // set not empty file path to parse
	const std::string pathInFile[] = { "test/VeryLongPathName", "test/AnotherVeryVeryLongPathName.DLL" }; // set expected output

	// TC SCENARIO
	// open and parse valid file 
	CParser parser(parserFileName); // try to parse the file

	for (unsigned int i = 0; i < parser.getPaths().size(); ++i)
	{
		EXPECT_EQ(parser.getPaths()[i], pathInFile[i]);
	}

	// POSTCONDITIONS
	// file is opened and parsed properly, files paths from parsed file are stored is std::vector as a parser member
}

TEST(Parser, doNotOpenNonExistingFile)
{
	// PRECONDITIONS
	// parser data is delivered, file does not exist
	const std::string parserFileName = ""; // empty file path

	// TC SCENARIO
	// do not open and parse invalid file
	try {
		CParser parser(parserFileName); // try to parse the file
		FAIL(); // fail the test if opened and parsed invalid file
	}
	catch (std::exception) {}

	// POSTCONDITIONS
	// file is not opened nor parsed, exception is thrown
}

TEST(Parser, parseEmptyFile)
{
	// PRECONDITIONS
	// parser data is delivered, file is empty
	const std::string parserEmptyFileName = "emptyFile.txt"; // set empty file path to parse

	// TC SCENARIO
	// when file contains no data set it to an empty string
	CParser parser(parserEmptyFileName); // try to parse the file
	EXPECT_EQ(parser.getPaths()[0], ""); // expect an empty parser file path

	// POSTCONDITIONS
	// empty file is parser normally, empty string is stored in paths vector (file won't be loaded anyway)
}
