#include "gtest\gtest.h"

#include "Base64_CStoper.h"

#include <Windows.h>

using namespace Base64_Application;
using namespace Base64_Time;

TEST(Stoper, countElapsedTime)
{
	// PRECONDITIONS
	const unsigned int error = 2; // set stoper miliseconds error
	const unsigned int sleepTime = 200; // set time to count
	__int64 elapsed; // declare elapsed time

	// TC SCENARIO
	// stoper should count elapsed time properly
	CStoper<std::chrono::milliseconds> testStoper; // declare stoper and start counting time
	Sleep(sleepTime); // sleep time for stoper to count
	elapsed = testStoper.elapsed().count(); // count elapsed time
	ASSERT_TRUE(elapsed > sleepTime - error || elapsed < sleepTime + error); // check if elapsed time is between the range

	// POSTCONDITIONS
	// correct elapsed time is returned
}
