#include "gtest\gtest.h"
#include "gmock\gmock.h"

#define private public   // for testing public methods which are operating on private field
#define protected public // for testing public methods which are operating on protected field

#include "Base64_ITask.h"
#include "Base64_CTaskQueue.h"
#include "Base64_CThreadPool.h"
#include "Tests_MockTask.h"

using namespace Base64_Application;
using namespace Base64_Tasks;
using namespace Base64_Thread_Pool;
using namespace Tests;

TEST(TaskQueue, settingQueueDone)
{
	// PRECONDITIONS
	// task queue is created
	CTaskQueue taskQueue; // create new task queue

	// TC SCENARIO
	// when creating new task queue its done flag should be set to false, when function setDone() is called it should change to true
	EXPECT_FALSE(taskQueue.m_done); // first check if false
	taskQueue.setDone(); // execute function
	EXPECT_TRUE(taskQueue.m_done); // it should be set to true

	// POSTCONDITIONS
	// newly created task queue is not marked as done (still more data to come)
}

TEST(TaskQueue, addingTask)
{
	// PRECONDITIONS
	// task queue is created
	CTaskQueue taskQueue; // create new task queue

	// TC SCENARIO
	// when task queue is created it should be empty
	EXPECT_TRUE(taskQueue.m_tasks.empty()); // check if empty
	taskQueue.addTask(new MockTask()); // add new task to the queue
	EXPECT_FALSE(taskQueue.m_tasks.empty()); // now check if queue not empty
	EXPECT_EQ(taskQueue.m_tasks.size(), 1); // check if queue size is equal to 1

	// POSTCONDITIONS
	// newly created task queue is initialized empty
}

TEST(TaskQueue, gettingTask)
{
	// PRECONDITIONS
	// a task queue is created
	Base64_Application::Base64_Tasks::CTaskQueue taskQueue; // create a new task queue
	ITask *task = NULL; // declare a task to assign it a pointer from the queue
	ITask *toAddTask1 = new MockTask(); // a new task to add to the queue
	ITask *toAddTask2 = new MockTask();

	// TC SCENARIO
	// return a valid task pointer from the queue
	taskQueue.addTask(toAddTask1); // add tasks to the queue
	taskQueue.addTask(toAddTask2);
	task = taskQueue.getTask(); // get a task from the queue

	EXPECT_TRUE(toAddTask1 == task); // check if the task pointer is ok
	EXPECT_FALSE(toAddTask2 == task); // this should be false - FIFO queue

	delete task;
	task = NULL;
	delete toAddTask2;
	toAddTask2 = NULL;
	//no need to delete toAddTask1 - task deleted

	// POSTCONDITIONS
	// a valid task pointer taken from the queue is returned
}
