#include "gtest\gtest.h"
#include "gmock\gmock.h"

#define private public   // for testing public methods which are operating on private field
#define protected public // for testing public methods which are operating on protected field

#include "Base64_CThreadPool.h"
#include "Tests_MockTask.h"
#include "Tests_StubThreadPoolInitFail1.h"
#include "Tests_StubThreadPoolInitFail2.h"
#include "Tests_StubThreadPoolWorkingThread.h"
#include "Tests_StubTerminateTask.h"
#include "Tests_StubEmptyTask.h"

using namespace Base64_Application;
using namespace Base64_Thread_Pool;
using namespace Tests;

TEST(ThreadPool, initTooManyThreads)
{
	// PRECONDITIONS
	// -
	int nThreads = 100; // set number of threads

	// TC SCENARIO
	// user can't create threadpool with more than MAX threads, set value to MAX
	CThreadPool threadPool(nThreads); // create ThreadPool with too many threads
	EXPECT_EQ(threadPool.m_nThreads, threadPool.MAX_THREADS); // check if number of threads is sets to MAX_THREADS value
	threadPool.setQueueDone(); // mark queue as done
	threadPool.closeHandlers(); // close threads

	// POSTCONDITIONS
	// threadpool is created with MAX number of threads
}

TEST(ThreadPool, initNegativeNumberOfThreads)
{
	// PRECONDITIONS
	// -
	int nThreads = -100; // set number of threads

	// TC SCENARIO
	// user can't create threadpool with negative number of threads, set it to MIN value
	CThreadPool threadPool(nThreads); // create ThreadPool with too negative threads value
	EXPECT_EQ(threadPool.m_nThreads, threadPool.MIN_THREADS); // check if number of threads is sets to MIN_THREADS value
	threadPool.setQueueDone(); // mark queue as done
	threadPool.closeHandlers(); // close threads

	// POSTCONDITIONS
	// threadpool is created with MIN number of threads
}

TEST(ThreadPool, initZeroThreads)
{
	// PRECONDITIONS
	// -
	int nThreads = 0; // set number of threads

	// TC SCENARIO
	// user can't create threadpool with 0 threads, set it to MIN value
	CThreadPool threadPool(nThreads); // create ThreadPool with 0 threads
	EXPECT_EQ(threadPool.m_nThreads, threadPool.MIN_THREADS); // check if number of threads is sets to MIN_THREADS value
	threadPool.setQueueDone(); // mark queue as done
	threadPool.closeHandlers(); // close threads

	// POSTCONDITIONS
	// threadpool is created with MIN number of threads
}

TEST(ThreadPool, deleteHandlers)
{
	// PRECONDITIONS
	// thread pool is created
	int nThreads = 10; // set number of threads
	CThreadPool threadPool(nThreads); // create ThreadPool

	// TC SCENARIO
	// delete handlers when threads are done
	EXPECT_TRUE(NULL != threadPool.m_threads);
	threadPool.setQueueDone();
	threadPool.closeHandlers();
	EXPECT_TRUE(NULL == threadPool.m_threads);

	// POSTCONDITIONS
	// threadpool threads handlers are deleted properly
}

TEST(ThreadPool, addAndExecuteTask)
{
	// PRECONDITIONS
	// thread pool is created, task is created
	int nThreads = 1; // set number of threads
	CThreadPool threadPool(nThreads); // create ThreadPool
	MockTask *task = new MockTask(); // create MockTest

	// TC SCENARIO
	// when task has been added to the task queue by threadpool it should be automatically executed
	EXPECT_CALL(*(task), execute()).Times(1); // check if task's execute method has been called
	threadPool.addTask(task); // add task to queue and execute it
	threadPool.setQueueDone(); // mark queue as done
	threadPool.closeHandlers(); // close threads

	// POSTCONDITIONS
	// task which was added to queue is executed correctly
}

TEST(ThreadPool, stubThrowExcFromInitThreads)
{
	// PRECONDITIONS
	// thread pool is created
	int nThreads = 1; // set number of threads
	StubThreadPoolInitFail1 threadPool; // create stub ThreadPool

	// TC SCENARIO
	// do not create thread pool when memory allocation failed in initThreads method
	try {
		threadPool.initThreads(nThreads);
		FAIL(); // fail when threadPool created
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	// POSTCONDITIONS
	// memory allocation failed - thread pool not created
}

TEST(ThreadPool, stubCreatingHandlesFailed)
{
	// PRECONDITIONS
	// thread pool is created
	int nThreads = 1; // set number of threads
	StubThreadPoolInitFail2 threadPool; // create stub ThreadPool

	// TC SCENARIO
	// do not create thread pool when memory allocation failed in initThreads method
	try {
		threadPool.initThreads(nThreads);
		FAIL(); // fail when threadPool created
	}
	catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}

	// POSTCONDITIONS
	// handlers creating failed - do not create thread pool
}

TEST(ThreadPool, stubWorkingThreadFunction)
{
	// PRECONDITIONS
	// thread pool is created, threads are initialized
	int nThreads = 10; // set number of threads
	StubThreadPool threadPool; // create ThreadPool
	threadPool.initThreads(nThreads);

	// TC SCENARIO
	// check if threads are executing (returns 0) and closing correctly (handlers set to null)
	
	threadPool.closeHandlers(); // close threads		
	EXPECT_TRUE(NULL == threadPool.m_threads); // check if threads handlers array is set to null

	// POSTCONDITIONS
	// threads working function return correctly
}

TEST(ThreadPool, stubTerminateThreadsWithSignal)
{
	// PRECONDITIONS
	// thread pool is created, termination signal is caught
	int nThreads = 5; // set number of threads
	CThreadPool threadPool(nThreads); // create ThreadPool

	// TC SCENARIO
	// check if threads are terminating properly after ctrl+C signal is caught
	threadPool.addTask(new EmptyTask()); // add few tasks to queue and execute it
	threadPool.addTask(new EmptyTask());
	threadPool.addTask(new EmptyTask());
	threadPool.addTask(new EmptyTask());
	threadPool.addTask(new TerminateTask());

	threadPool.setQueueDone(); // mark queue as done
	threadPool.closeHandlers(); // close threads
	EXPECT_TRUE(NULL == threadPool.m_threads); // check if threads handlers array is set to null

	// POSTCONDITIONS
	// threads are terminated properly when termination signal is caught
}
