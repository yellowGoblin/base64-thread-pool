#include "gtest\gtest.h"
#include "gmock\gmock.h"

#define private public   // for testing public methods which are operating on private field
#define protected public // for testing public methods which are operating on protected field

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	testing::InitGoogleMock(&argc, argv);

	int testReturn = RUN_ALL_TESTS();

	return testReturn;

	return 0;
}